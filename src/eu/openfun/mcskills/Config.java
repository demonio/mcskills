package eu.openfun.mcskills;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Config {
	public String server;
	public String db;
	public String username;
	public String password;
	
	public void read() {
		
		BufferedReader in = null;
		
		File theDir = new File("config/MCSkills");
		if (!theDir.exists()) {
		    try{
		        theDir.mkdir();
		    } 
		    catch(SecurityException se){
		        se.printStackTrace();
		    }        
		}
		
		File f = new File("config/MCSkills/config.txt");
		if(!f.exists())
			try {
				f.createNewFile();
				server = "localhost";
				db = "mcskills";
				username = "root";
				password = "password";
				FileWriter f1 = new FileWriter("config/MCSkills/config.txt", true);
				f1.write(server + System.getProperty("line.separator"));
				f1.write(db + System.getProperty("line.separator"));
				f1.write(username + System.getProperty("line.separator"));
				f1.write(password + System.getProperty("line.separator"));
				f1.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		int i = 0;
		FileReader fr = null;
		try {
			fr = new FileReader("config/MCSkills/config.txt");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		in = new BufferedReader(fr);
		
		try {
			while (in.ready()) {
				i++;
				if(i == 1)
					server = in.readLine();
				if(i == 2)
					db = in.readLine();
				if(i == 3)
					username = in.readLine();
				if(i == 4)
					password = in.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}