package eu.openfun.mcskills;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.canarymod.api.potion.PotionEffectType;
import net.canarymod.chat.MessageReceiver;

public class jobuCommand {
	public static Config config = new Config();
	
	public static void execute(MessageReceiver caller, String[] parameters) {
		config.read();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection connection = null;
		try {
			connection  = DriverManager
			.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM user WHERE name='"+caller.asPlayer().getName()+"'";
		
		Statement stmt =  null;
		String job = null;
		int lvl = 0;
		
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				job = rs.getString("job");
				
				lvl = rs.getInt("lvl");
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(lvl >= Integer.parseInt(parameters[1])){
			if(job.equals("trader")){
				caller.asPlayer().addPotionEffect(PotionEffectType.REGENERATION, Integer.parseInt(parameters[1])*255, 2);
			}
			else if(job.equals("miner")){
				caller.asPlayer().addPotionEffect(PotionEffectType.DIGSPEED, Integer.parseInt(parameters[1])*255, 2);
			}
			else if(job.equals("hunter")){
				caller.asPlayer().addPotionEffect(PotionEffectType.MOVESPEED, Integer.parseInt(parameters[1])*255, 2);
			}
			else if(job.equals("killer")){
				caller.asPlayer().addPotionEffect(PotionEffectType.DAMAGEBOOST, Integer.parseInt(parameters[1])*255, 2);
			}
			else if(job.equals("harvester")){
				caller.asPlayer().addPotionEffect(PotionEffectType.NIGHTVISION, Integer.parseInt(parameters[1])*255, 2);
			}
		}
		
		lvl -= Integer.parseInt(parameters[1]);
		
		String query1 = "UPDATE user SET lvl = '"+lvl+"' ";
		query1 += "WHERE name='"+caller.asPlayer().getName()+"'";
		
		try {
			stmt = connection.createStatement();
			stmt.executeUpdate(query1);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
