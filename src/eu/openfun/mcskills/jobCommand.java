package eu.openfun.mcskills;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import net.canarymod.chat.MessageReceiver;

public class jobCommand {
	private Statement stmt = null;
	private Connection connection = null;
	public static Config config = new Config();
	
	public void execute(MessageReceiver caller, String[] parameters) {
		config.read();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String query = "INSERT INTO user (name,job)";
		query += "VALUES ('"+caller.asPlayer().getName()+"', '"+parameters[1]+"')";
		query += "ON DUPLICATE KEY UPDATE ";
		query += " job = '"+parameters[1]+"';";
		
		try {
			//caller.asPlayer().message(query);
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		caller.asPlayer().message("You got "+parameters[1]+" Job");
	}

}
