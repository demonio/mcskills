package eu.openfun.mcskills;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.canarymod.chat.MessageReceiver;

public class jobsCommand {
	private Statement stmt = null;
	private Connection connection = null;
	public static Config config = new Config();
	
	public void execute(MessageReceiver caller, String[] parameters) {
		config.read();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM user WHERE name='"+caller.asPlayer().getName()+"'";
		
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				caller.asPlayer().message("Your job is "+ rs.getString("job"));
				caller.asPlayer().message("You need "+ rs.getInt("next")+" exp to next lvl");
				caller.asPlayer().message("Your lvl is "+ rs.getInt("lvl"));
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
