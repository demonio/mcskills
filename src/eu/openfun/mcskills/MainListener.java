package eu.openfun.mcskills;

import java.io.IOException;
import java.util.Random;

import net.canarymod.Canary;
import net.canarymod.api.entity.EntityType;
import net.canarymod.api.entity.living.humanoid.Player;
import net.canarymod.api.inventory.ItemType;
import net.canarymod.api.world.blocks.BlockType;
import net.canarymod.commandsys.commands.vanilla.Message;
import net.canarymod.hook.HookHandler;
import net.canarymod.hook.entity.EntityDeathHook;
import net.canarymod.hook.player.BlockDestroyHook;
import net.canarymod.hook.player.ItemUseHook;
import net.canarymod.hook.player.VillagerTradeHook;
import net.canarymod.plugin.PluginListener;
import net.visualillusionsent.dconomy.accounting.AccountNotFoundException;
import net.visualillusionsent.dconomy.accounting.AccountingException;
import net.visualillusionsent.dconomy.api.InvalidPluginException;
import net.visualillusionsent.dconomy.api.account.wallet.WalletAPIListener;
import net.visualillusionsent.dconomy.canary.api.Canary_Server;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@SuppressWarnings("unused")
public class MainListener implements PluginListener {
	public int id;
	public String name;
	public String job;
	public int next;
	public int lvl;
	private Statement stmt = null;
	private Connection connection = null;
	public static Config config = new Config();
	
	public void get_user_data(){
		config.read();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM user WHERE name='"+name+"'";
		
		try {
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				id = rs.getInt("id");
				job = rs.getString("job");
				next = rs.getInt("next");
				lvl = rs.getInt("lvl");
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void clear_user_data(){
		name = null;
		id = 0;
		lvl = 0;
		next = 0;
		job = null;
	}
	
	private void set_user_data() {
		config.read();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String query = "UPDATE user SET job = '"+job+"', ";
		query += "next ='"+next+"', ";
		query += "lvl ='"+lvl+"' ";
		query += "WHERE name='"+name+"'";
		
		try {
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@HookHandler
    public void Trader(VillagerTradeHook hook) {
    	name = hook.getPlayer().getName();
    	get_user_data();
    	if(job == null){
    		hook.getPlayer().message("Use /job to get a job");
    	}else if(job.equals("trader")){
			int reputation = hook.getVillager().getVillage().getReputationForPlayer(hook.getPlayer());
			int villagers = hook.getVillager().getVillage().getVillagerCount();
			hook.getPlayer().message("Your reputation in this village is " + reputation + " and they have " + villagers + " villagers.");
			next -= 100;
			if (next < 1){
				lvl++;
				hook.getPlayer().message("You got new lvl " + lvl);
				next = lvl * lvl * 100;
				hook.getPlayer().message("You need " + next + " exp for next lvl");
				hook.getPlayer().addLevel(lvl);
				try {
					WalletAPIListener.walletDeposit("MCSkills", hook.getPlayer().getName(), (double) (lvl*lvl), false);
				} catch (AccountingException | AccountNotFoundException
						| InvalidPluginException e) {
					e.printStackTrace();
				}
			} else {
				hook.getPlayer().message("You need " + next + " exp for next lvl");
			}
		}
		
		set_user_data();
		clear_user_data();
	}
	
	@SuppressWarnings("deprecation")
	@HookHandler
    public void Miner(BlockDestroyHook hook) {
		name = hook.getPlayer().getName();
    	get_user_data();
    	if(job == null){
    		hook.getPlayer().message("Use /job to get a job");
    	}else if(job.equals("miner")){
    		if (hook.getBlock().getIdDropped() == 264){
				next -= 10;
			} else if (hook.getBlock().getType() == BlockType.EmeraldOre){
				next -= 5;
			} else if (hook.getBlock().getType() == BlockType.IronOre || hook.getBlock().getType() == BlockType.GoldOre) {
				next -=2;
			} else if (hook.getBlock().getType() == BlockType.RedstoneOre || hook.getBlock().getType() == BlockType.CoalOre) {
				next -=1;
			}
    		if (next < 1){
				lvl++;
				hook.getPlayer().message("You got new lvl " + lvl);
				next = lvl * lvl * 100;
				hook.getPlayer().message("You need " + next + " exp for next lvl");
				hook.getPlayer().addLevel(lvl);
				try {
					WalletAPIListener.walletDeposit("MCSkills", hook.getPlayer().getName(), (double) (lvl*lvl), false);
				} catch (AccountingException | AccountNotFoundException
						| InvalidPluginException e) {
					e.printStackTrace();
				}
			}
    	}
    	
		set_user_data();
		clear_user_data();
	}
	
	@HookHandler
    public void Hunter(EntityDeathHook hook) {
		if (hook.getDamageSource().getDamageDealer() == null)
			return;
		if (hook.getDamageSource().getDamageDealer().getEntityType() != EntityType.PLAYER)
			return;
		name = hook.getDamageSource().getDamageDealer().getName();
		Player player = (Player) hook.getDamageSource().getDamageDealer();
		get_user_data();
		
		if(job == null){
    		player.message("Use /job to get a job");
    	}else if(job.equals("hunter")){
    		if (hook.getEntity().getEntityType() == EntityType.OCELOT || hook.getEntity().getEntityType() == EntityType.RABBIT || hook.getEntity().getEntityType() == EntityType.VILLAGER){
				next -= 10;
			} else if (hook.getEntity().getEntityType() == EntityType.COW || hook.getEntity().getEntityType() == EntityType.SHEEP || hook.getEntity().getEntityType() == EntityType.SQUID){
				next -= 5;
			} else if (hook.getEntity().getEntityType() == EntityType.GENERIC_ANIMAL) {
				next -= 2;
			}
    		if (next < 1){
				lvl++;
				player.message("You got new lvl " + lvl);
				next = lvl * lvl * 100;
				player.message("You need " + next + " exp for next lvl");
				player.addLevel(lvl);
				try {
					WalletAPIListener.walletDeposit("MCSkills", player.getName(), (double) (lvl*lvl), false);
				} catch (AccountingException | AccountNotFoundException
						| InvalidPluginException e) {
					e.printStackTrace();
				}
			}
    	}
		
		set_user_data();
		clear_user_data();
	}
	
	@HookHandler
	public void Killer(EntityDeathHook hook) {
		if (hook.getDamageSource().getDamageDealer() == null)
			return;
		if (hook.getDamageSource().getDamageDealer().getEntityType() != EntityType.PLAYER)
			return;
		name = hook.getDamageSource().getDamageDealer().getName();
		Player player = (Player) hook.getDamageSource().getDamageDealer();
		get_user_data();
		
		if(job == null){
    		player.message("Use /job to get a job");
    	}else if(job.equals("killer")){
    		if (hook.getEntity().getEntityType() == EntityType.ENDERDRAGON || hook.getEntity().getEntityType() == EntityType.WITHER || hook.getEntity().getEntityType() == EntityType.GUARDIAN){
				next -= 100;
			} else if (hook.getEntity().getEntityType() == EntityType.ENDERMAN || hook.getEntity().getEntityType() == EntityType.GHAST || hook.getEntity().getEntityType() == EntityType.WITCH || hook.getEntity().getEntityType() == EntityType.PLAYER){
				next -= 5;
			} else if (hook.getEntity().getEntityType() == EntityType.BLAZE || hook.getEntity().getEntityType() == EntityType.SKELETON || hook.getEntity().getEntityType() == EntityType.ZOMBIE || hook.getEntity().getEntityType() == EntityType.SPIDER) {
				next -=1;
			}
    		if (next < 1){
				lvl++;
				player.message("You got new lvl " + lvl);
				next = lvl * lvl * 100;
				player.message("You need " + next + " exp for next lvl");
				player.addLevel(lvl);
				try {
					WalletAPIListener.walletDeposit("MCSkills", player.getName(), (double) (lvl*lvl), false);
				} catch (AccountingException | AccountNotFoundException
						| InvalidPluginException e) {
					e.printStackTrace();
				}
			}
    	}
		
		set_user_data();
		clear_user_data();
	}
	
	@SuppressWarnings("deprecation")
	@HookHandler
	public void Harvester(BlockDestroyHook hook) {
		name = hook.getPlayer().getName();
    	get_user_data();
    	if(job == null){
    		hook.getPlayer().message("Use /job to get a job");
    	}else if(job.equals("harvester")){
    		if(hook.getBlock().getIdDropped() == 391 || hook.getBlock().getIdDropped() == 392 || hook.getBlock().getIdDropped() == 295){
    			next -= 2;
    		} else if (hook.getBlock().getIdDropped() == 394 || hook.getBlock().getIdDropped() == 39 || hook.getBlock().getIdDropped() == 40){
    			next -= 5;
    		} else if (hook.getBlock().getIdDropped() == 360){
    			next -= 1;
    		}
    		if (next < 1){
				lvl++;
				hook.getPlayer().message("You got new lvl " + lvl);
				next = lvl * lvl * 100;
				hook.getPlayer().message("You need " + next + " exp for next lvl");
				hook.getPlayer().addLevel(lvl);
				try {
					WalletAPIListener.walletDeposit("MCSkills", hook.getPlayer().getName(), (double) (lvl*lvl), false);
				} catch (AccountingException | AccountNotFoundException
						| InvalidPluginException e) {
					e.printStackTrace();
				}
			}
    	}
    	
		set_user_data();
		clear_user_data();
	}
}
