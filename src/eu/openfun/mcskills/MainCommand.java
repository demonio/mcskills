package eu.openfun.mcskills;

import net.canarymod.chat.MessageReceiver;
import net.canarymod.commandsys.Command;
import net.canarymod.commandsys.CommandListener;

public class MainCommand implements CommandListener {
    private final jobCommand jobCommand = new jobCommand();
    private final jobsCommand jobsCommand = new jobsCommand();
	
    @Command(aliases = { "job" },
            description = "select your profesion",
            permissions = { "openfun.command.job", "openfun.command.job" },
            toolTip = "/job [trader|miner|hunter|killer|harvester]",
            min = 2)
    public void jobCommand(MessageReceiver caller, String[] parameters) {
    	jobCommand.execute(caller, parameters);
    }
    @Command(aliases = { "jobstat" },
            description = "view your job stats",
            permissions = { "openfun.command.job", "openfun.command.job" },
            toolTip = "/jobstat",
            min = 1)
    public void jobstatCommand(MessageReceiver caller, String[] parameters) {
    	jobsCommand.execute(caller, parameters);
    }
    @Command(aliases = { "jobuse" },
            description = "consume your lvls to get bonus",
            permissions = { "openfun.command.job", "openfun.command.job" },
            toolTip = "/jobuse [amount] - amount of lvls to consume",
            min = 2)
    public void jobuseCommand(MessageReceiver caller, String[] parameters) {
    	jobuCommand.execute(caller, parameters);
    }
}
